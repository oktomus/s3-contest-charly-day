<?php
namespace app\utils;

/**
 * Classe qui permet de gérer les exceptions lancées par Authentification, elle hérite de Exception
 */
class ConnexionException extends \Exception {

  public function __construct($message, $code = 0) {

    parent::__construct($message, $code);

  }

  public function __toString() {

    return $this->message;

  }

}