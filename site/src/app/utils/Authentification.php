<?php

namespace app\utils;

use app\models\Prestataire;

class Authentification{


  public static function createUser(Prestataire $u, $pwd){
    $hash = password_hash($pwd, PASSWORD_DEFAULT, array('cost'=>12));
    $u->mdp = $hash;
    $u->save();
  }

  public static function authenticate($username, $pwd){
    $user = Prestataire::where('email', '=', $username)->first();
    if(is_null($user)){
      throw new ConnexionException("Utilisateur : ". $username ." inexistant.", 1);
    }else{
      if(password_verify($pwd, $user->mdp)){
        Authentification::loadProfile($user->email);
      }else{
        throw new ConnexionException("Mot de passe incorrect.", 1);
      }
    }
  }

  private static function loadProfile($uid){
    $user = Prestataire::find($uid);
    $_SESSION['email'] = $uid;
    $_SESSION['client_ip'] = $_SERVER['REMOTE_ADDR'];
    $_SESSION['auth-level'] = 1;
    $_SESSION['dernierActivite'] = time();
    $_SESSION['dateCreationSession'] = time();

  }

  /**
   * Fonction qui permet de vérifier l'état de la session
   * vérification de l'adresse IP, vérification du temps de non activité (temps > 30 min entraine une déconnexion)
   * regénération de l'id de session au bout de 10 min
   * @throws ConnexionException exception lancée si la vérification a échoué avec un message personnalisé
   */
  public static function checkSession() {
    
    if (isset($_SESSION['email'])) {
      if ($_SESSION['client_ip'] == $_SERVER['REMOTE_ADDR']) { // verifier IP
        if (isset($_SESSION['dernierActivite']) && (time() - $_SESSION['dernierActivite'] > 1800)) { // verifier time stamp
          $message = <<<END
      <p class="light red-text">TIME OUT. Vous n'avez pas eu d'activité depuis plus de 30 minutes.</p>
END;
          self::deconnexion();
      throw new ConnexionException($message, 1);
        } else { // cas où tout se passe bien
          $_SESSION['dernierActivite'] = time();
        
          if (time() - $_SESSION['dateCreationSession'] > 600) {
              // session débutée il y a plus de 10 minutes
              session_regenerate_id(true);
              $_SESSION['dateCreationSession'] = time();
          }

        }
      } else {
        $message = <<<END
      <p class="light red-text">Vous avez changé d'adresse IP. Veuillez vous reconnecter.</p>
END;
      self::deconnexion();
      throw new AuthException($message, 1);
      }
    } else {
      $message = <<<END
      <p class="light">Vous n'êtes pas connecté. Veuillez vous connecter.</p>
END;
      
      self::deconnexion();
      throw new ConnectionException($message, 1);
    }
      
  }

  public static function checkAccessRight($required){
    if(isset($_SESSION['auth-level'])){
      if($_SESSION['auth-level'] < $required){
        return false;
      }else{
        return true;
      }
    }else{
      return false;
    }
  }

  public static function deconnexion(){
    foreach($_SESSION as $key => $value){
      unset($_SESSION[$key]);
    }
  }
}
