<?php

namespace app\utils;

use \app\models\Pochette;

class GestionPochette{


  public static function creePochette(Pochette $p){
    $p->urlGestion =   md5("".uniqid());
    $p->urlCagnotte = md5("cagnotte_".uniqid());
    $p->urlCadeau = md5("surprise".uniqid());
    $p->save();
    self::loadProfile($p->id, 2);
  }



  public static function loadProfile($id, $role){
    $_SESSION['idPochette'] = $id;
    $_SESSION['role'] = $role;

  }

  public static function roleUtilisateur(){
    if(isset($_SESSION['role'])){
      return $_SESSION['role'];
      }
      return -1;
  }


  public static function pochetteUtilisateur(){
    if(isset($_SESSION['idPochette'])){
      return $_SESSION['idPochette'];
      }
      return -1;
  }

  public static function deconnexion(){
    foreach($_SESSION as $key => $value){
      unset($_SESSION[$key]);
    }
  }
}
