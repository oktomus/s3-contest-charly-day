<?php

namespace app\vue;

use app\utils\Authentification;



class VueIncludes{

	private $data;
	private $content;

	public function __construct($data = null){
		$this->data = $data;
	}

	public function render($type){
		switch ($type) {
			default:
			case 1:
		    $content = $this->head();
        break;
			case 2:
				$content = $this->menu();
				break;
			case 3:
				$content = $this->piedDePage();
				break;
			case 4:
				$content = $this->foot();
				break;
		}
		echo "$content";
	}

	private function menu(){
		// APP
		$app = \Slim\Slim::getInstance();
    $uri = $app->request->getRootUri();
		// LOGO
		$home = "<a class=\"brand-logo\" href=\"". $app->urlFor('home') ."\">App</a>";
		// MAIN MENU
		$r = \Slim\Environment::getInstance();
		$pageCourante = $r['PATH_INFO'];

		$normal = array("home"=>"Accueil","pochette"=>"Ma Pochette","catalogue"=>"Catalogue");
		$prestataire = $normal;
		$prestataire["ajouterNouvellePrestation"] = "Ajouter une prestation";
		$prestataire["deconnecter"] = "Se déconnecter";

		if (isset($_SESSION['email'])){
			$normal = $prestataire;
		}
		$menu = "";
		foreach($normal as $k=>$v){
			if ("/".$k == $pageCourante){
				$menu .="<li class=\"active\" ><a href=\"". $app->urlFor($k) ."\">$v</a></li>";
			}else{
				$menu .="<li class=\"\" ><a href=\"". $app->urlFor($k) ."\">$v</a></li>";
			}
		}

		
		$res = <<<END
			<nav class="amber darken-3">
				<div class="container nav-wrapper">
					$home
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="grey-text text-darken-4 material-icons">menu</i></a>
					<ul class="right hide-on-med-and-down">

						$menu
					</ul>
					<ul id="mobile-demo" class="side-nav">
						$menu
					</ul>
				</div>
			</nav>

END;
			return $res;
	}

  private function foot(){
		$app = \Slim\Slim::getInstance();
    $uri = $app->request->getRootUri();

    $content = <<<END
    		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    		<script src="$uri/public/js/init.js" type="text/javascript"></script>
    		<script src="$uri/public/js/materialize.js" type="text/javascript"></script>
    		<script src="$uri/public/js/carousel.js" type="text/javascript"></script>
    		<script src="$uri/public/js/masonry.js" type="text/javascript"></script>
			<script src="$uri/public/js/catalogue.js" type="text/javascript"></script>
		</body>
    </html>
END;
    return $content;

  }

	private function piedDePage(){
		$app = \Slim\Slim::getInstance();
		$lien = $app->urlFor('prestataire');
		$liens = "";
		if (!isset($_SESSION['email'])) {
			$liens = <<<END
			<div class="row">
              <div class="col l6 s12">
                <p class="grey-text text-lighten-4">Vous êtes prestataire ? Cliquez <a style="color:white;text-decoration:underline" href="$lien">ici</a>.</p>
              </div>
              <div class="col l4 offset-l2 s12">
              </div>
            </div>
END;
		}

		$res = <<<END
		<footer class="page-footer amber darken-3">
		<div class="container">
            $liens
          </div>
			<div class="footer-copyright">
        <div class="container">
          Codé avec ♥ par Les demi-mangeur de demi-pizza © 2016 Tous droits réservés
        </div>
      </div>
    </footer>

END;
		return $res;
	}

  private function head(){
		$app = \Slim\Slim::getInstance();
    $uri = $app->request->getRootUri();
    $content = <<<END
		<!DOCTYPE html>
		<html lang="fr">
		  <head>
		    <meta charset="utf-8">
		    <title> Lucky Bag </title>
				<link rel="icon" href="$uri/public/favicon.ico"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			<link href="$uri/public/css/materialize.css" type="text/css" rel="stylesheet" />
			<link href="$uri/public/css/materialize.min.css" type="text/css" rel="stylesheet" />
			<link type="text/css" rel="stylesheet" href="$uri/public/css/app.css"/>

		  </head>
		  <body>
END;
      return $content;

  }


}
