<?php

namespace app\vue;

class VuePrestataire {

	private $data;
	private $content;

	public function __construct($data = null){
		$this->data = $data;
	}

	public function render($type){
		switch ($type) {
			case 1:
				$content = $this->formulaireConnexion();
				break;
			case 2:
				$content = $this->formulaireAjoutPrestation();
				break;
			case 3:
				$content = $this->ajoutEffectue();
				break;
		}
		if (isset($this->data['message']) && !is_null($this->data['message'])) {
			$mess = $this->data['message'];
			$content .= <<<END
				<div class="row">
					<p class="red-text center-align">$mess</p>
				</div>
END;
		}
		echo $content;
    }

  	private function formulaireConnexion(){

		$app = \Slim\Slim::getInstance();
    	$uri = $app->request->getRootUri();
  		$html = <<<END
  		<div class="row">
	  		<form class="col s12" action="connecterPrestataire" method="POST">
	  			<br><br>
	  			<div class="row">
			        <div class="input-field col s12 m12 l4 offset-l4">
			          <i class="material-icons prefix">account_circle</i>
			          <input id="email" name="email" type="email" class="validate" required>
			          <label for="email">Adresse email</label>
			        </div>
			        <div class="input-field col s12 m12 l4 offset-l4">
			          <i class="material-icons prefix">vpn_key</i>
			          <input id="last_name" name="mdp" type="password" class="validate" required>
			          <label for="last_name">Mot de passe</label>
			        </div>
			      </div>
			    </div>
			    <div class="row">
			    	<button class="waves-effect waves-light btn col s12 m12 l4 offset-l4 black" type="submit" name="connection" value="connection"><i>Se connecter</button>
			    </div>
	  		</form>
  		</div>

	  	<div class="row center-align">
	  		<img src="$uri/public/background/background2_1.jpg" alt="img1" width="50%">
	  	</div>
END;


    return $html;
  }

  private function formulaireAjoutPrestation() {

	$app = \Slim\Slim::getInstance();
    $uri = $app->request->getRootUri();
  	$select = <<<END
  		<select name='type' class="browser-default amber lighten-5" required>
			<option disabled>Choisir le type</option>
					
END;

	if (isset($this->data['message']))
		unset($this->data['message']);
  	foreach ($this->data as $key => $value) {
  		$select .= "<option value='" . $value['id'] . "'>" . $value['nom'] ."</option>";

  	}
  	$select .= "</select>";
  	$html = <<<END
  		<div class="row">
	  		<form action="ajouter" method="POST">
	  			<br><br>
	  			<div class="col l12 s12 m12">
			        <div class="input-field col s12 m12 l6">
			          <i class="material-icons prefix">visibility</i>
			          <input id="nom" name="nom" type="text" class="validate" required>
			          <label for="nom">Nom</label>
			        </div>
			        <div class="input-field col s12 m12 l6">
			          <i class="material-icons prefix">shopping_basket</i>
			          <input id="prix" name="prix" type="number" step="0.01" class="validate" required>
			          <label for="prix">Prix</label>
				    </div>
				</div>
		        <div class="input-field col s12 m6 l6">
		          <div class="file-field input-field">
			      	<div class="waves-effect waves-light btn black col l4 s8 m6">
			        	<span>Image</span>
			        	<input type="file" name="img">
			      	</div>
			      	<div class="file-path-wrapper">
			        	<input class="file-path validate" type="text" disabled>
			      	</div>
			      </div>
			    </div>
			    <div class="row col l6 m6 s12">
			    	$select
			    </div>
			    <div class="row">
			        <div class="input-field col s12 m12 l12">
			          <i class="material-icons prefix">description</i>
			          <input id="description" name="description" type="text" class="validate" required>
			          <label for="description">Description</label>
			        </div>
			    </div>
			    <div class="row">
			    	<button class="waves-effect waves-light btn col s12 m12 l4 offset-l4 black" type="submit" name="ajouterPres" value="ajouterPres"><i>Ajouter la prestation</button>
			    </div>
	  		</form>
  		</div>

	  	<div class="row center-align">
	  		<img src="$uri/public/background/background2_1.jpg" alt="img1" width="50%">
	  	</div>
END;

	return $html;
  }

  public function ajoutEffectue() {

	$app = \Slim\Slim::getInstance();
    $uri = $app->request->getRootUri();
  	
  	$nom = $this->data['nom'];
  	$type = $this->data['type'];
  	$prix = $this->data['prix'];
  	$descri = $this->data['descr'];
  	$html = <<<END
  	<div class="row">
	  	<p class="ligth center-align">
	  	La création d'une nouvelle prestation a bien été réalisée. Elle a pour nom : $nom, pour prix : $prix, pour type : $type et pour description : $descri .
	  	</p>
	  	<div class="row center-align">
	  		<img src="$uri/public/background/background2_1.jpg" alt="img1" width="50%">
	  	</div>
  	</div>
END;
	return $html;
  }

}
