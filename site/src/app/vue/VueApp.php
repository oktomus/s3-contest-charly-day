<?php

namespace app\vue;



class VueApp{

	private $data;
	private $content;

	public function __construct($data = null){
		$this->data = $data;
	}

	public function render($type){
		switch ($type) {
			default:
			case 1:
				$content = $this->home();
				break;
			case 2:
				$content = $this->notFound();
				break;
		}
		echo $content;
	}



	private function home(){
		return "<div class=\"container\">HOMMMMMMMMME</div>";
	}

	private function notFound(){
		return '404';
	}


}
