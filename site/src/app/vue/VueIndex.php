<?php

namespace app\vue;

use app\utils\Authentification;

class VueIndex {

	private $data;
	private $content;

	public function __construct($data = null){
		$this->data = $data;
	}

	public function render($type){
		switch ($type) {
			case 1:
		    	$content = $this->accueil();
        		break;
        	case 2:
        		$content = $this->accueilDetail();
        		break;
		}
		echo "$content";
	}

	public function accueil() {
		$app = \Slim\Slim::getInstance();
    $uri = $app->request->getRootUri();
    $uri2 = $app->urlFor("catalogue");
    $uriImg = $uri . "/public/img/";

    $slider = <<<END

<div class="slider">
    <ul class="slides">
END;
    $alea = 1;
    foreach ($this->data as $type => $presta) {
      $slider .= '<li><img src="' . $uriImg . $presta['img'] . '" alt="pouet"><div class="caption center-align"> <h3 class="light amber-text text-darken-3">' . $type . '</h3><h3 class="amber-text text-darken-3">' . $presta['nom'] . '</h3><h5 class="light amber-text text-darken-3">' . $presta['descr'] . '</h5></div></li>';
    }

    $slider .= <<< END

    </ul>
  </div>
END;


		$html = <<<END
	<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">

        <h2 class="header center amber-text text-darken-3">
          <img alt="logo" src="$uri/public/media/logo.png" class="responsive-img" width="500" height="250">
        </h2>

        <div class="row center">
          <a href="$uri2" id="download-button" class="btn-large waves-effect waves-light amber darken-3">Parcourir notre catalogue</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="$uri/public/background/background1_2.jpg" alt="img1" width="50%"></div>
  </div>


  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center amber-text text-darken-3"><i class="material-icons amber-text text-darken-3">perm_media</i></h2>
            <h5 class="center">Parcourir nos prestations</h5>

            <p class="light">Un grand choix de prestations afin de satisfaire toute vos envies ! Pour n'importe quel moment de la journée, du plus déjanté au plus relaxant en passant par le plus gourmand, il y en a pour tous les goûts ! </p>

            
          </div>
        </div>

        <div id="experience" class="col s12 m4">
          <div class="icon-block">
            <h2 class="center amber-text text-darken-3"><i class="material-icons amber-text text-darken-3">grade</i></h2>
            <h5 class="center">Créer une pochette surprise</h5>
            
            <p class="light">Envie de surprises ? Constituez de toutes pièces une pochette surprise personnalisée avec les prestations de votre choix. Votre cadeau sera dévoilé au fur et à mesure que l'on découvre les activités ! </p>

    
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center amber-text text-darken-3"><i class="material-icons amber-text text-darken-3">perm_identity</i></h2>
            <h5 class="center">Offrir la pochette à un proche</h5>
            
            <p class="light">Et pourquoi pas une surprise pour un proche ? Idéale pour les anniversaires, enterrements de vie de jeune fille, et bien d'autres, la pochette surprise réserve des moments inoubliables ! </p>

          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="parallax-container valign-wrapper">
      <div class="parallax"><img src="$uri/public/background/background2_1.jpg" alt="img1"></div>
  </div>


    
    <div class="container">
    <div class="row">


$slider
  
</div>
</div>

<div class="parallax-container valign-wrapper">
  <div class="parallax"><img src="$uri/public/background/background3_1.jpg" alt="img1" width="50%"></div>
</div>

END;


		return $html;
	}

 }
