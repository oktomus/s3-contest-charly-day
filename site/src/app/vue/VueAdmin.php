<?php

namespace app\vue;



class VueAdmin{

	private $data;
	private $content;

	public function __construct($data = null){
		$this->data = $data;
	}

	public function render($type){
		switch ($type) {
			default:
			case 1:
				$content = $this->formulaireConnexion();
				break;
		}
    $message = "";
    if(isset($this->data) && isset($this->data['message']) && $this->data['message']){
       $message = "<div class=\"\"><div class=\"alert ". $this->data['message-type']."\" role=\"alert\">".$this->data['message-content']."</div></div>";
    }
		echo "<div class=\"container\">$message $content</div>";
	}

  private function formulaireConnexion(){
    return "connexion";
  }


}
