<?php

namespace app\vue;

use \app\models\Pochette;
use \app\models\PochettePresta;
use \app\models\Prestation;
use \app\models\Promesse;


class VuePochette
{


    private $data;
    private $content;

    public function __construct($data = null){
        $this->data = $data;
    }

    public function render($type){
        switch ($type) {
            default:
            case 1:
                $content = $this->nouvellePochette();
                break;
            case 2: //bonhomme = gerant
                $content = $this->pochette();
                break;
            case 3:
                $content = $this->notFound();
                break;
            case 4:
                $content = $this->paiementCagnotte();
                break;
        }
        $vue = new VueIncludes();
        $vue->render(1);
        $vue->render(2);
        echo "<div class=\"container\">".$content."</div>";
        $vue->render(3);
        $vue->render(4);
    }


    private function pochette(){
      $app = \Slim\Slim::getInstance();
      $total = 0;
      $idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
      $pp = PochettePresta::where('idPochette','=',$idPoch)->get();
      // CALCUL DU TOTAL
      foreach ($pp as $ppUnit) {
        $index = $ppUnit->idPrestation;
        $presta = Prestation::find($index);
        $total += intval($presta->prix);
      }
      $somme = 0;
      $promesses = Promesse::where('idPochette', '=', $idPoch)->get();
      // CALCUL DE LA SOMME DES DONS
      foreach ($promesses as $pUnit) {
        $somme += intval($pUnit->somme);
      }
      $html = "";
      $titre = "Pochette pour ". $this->data['pochette']->nom;
      $message = $this->data['pochette']->message;
      $lienImage = $this->data['public']."/media/share.jpg";
      $role = \app\utils\GestionPochette::roleUtilisateur();
      $lienImg = $app->request->getRootUri()."/public/img/";
      $boutonSuppr = "";
      $listeProduit = array();
      $pochette = Pochette::find($idPoch);
      $validation = "";

      $aCommande = PochettePresta::where('idPochette','=',$idPoch)->count()>0;
      $promesse = "";
      // SI CAGNOTTE ATTEITE
      if($somme>=$total && $total>0){
        $promesses .= '<h3 style="color:grey;text-align:left;" >Félicitation ! La cagnotte a été atteinte. </h3>';
      }else{
        // SI PAS CAGNOTTE ATTEINT
        $manque = intval($total)-intval($somme);
        // SI ON A DES PRESTA
        if($total>0){
          $promesses .= '<h3 style="color:grey;text-align:left;" >Il ne manque plus que ';
          $promesses .= $manque. '€ pour atteindre la cagnotte ! Nous avons besoin de votre aide !</h3>';
          $promesses .= '<div class="divider"></div><h5 style="color:grey;text-align:left;" >Faire une promesse</h5>';
          $promesses .= '<div class="section"><form action="" method="post">';
          $promesses .= '<div class="input-field col s6">
          <input type="hidden" name="faire" value="ajouterDon">
          <input type="hidden" name="pochette" value="'.$idPoch.'">
          <i class="material-icons prefix">account_circle</i>
          <input name="nom" required id="icon_prefix" type="text" placeholder="Jean" class="validate">
          <label for="icon_prefix">Nom</label>
          </div>';
          $promesses .= '<div class="input-field col s6">
          <i class="material-icons prefix">account_circle</i>
          <input name="montant" required id="montant" type="number" placeholder="10" class="validate">
          <label for="montant">Hauteur de votre participation (en euros)</label>
          </div>';
          $promesses .= '  <button class="btn green waves-effect waves-light" type="submit" name="action">Envoyer
          <i class="material-icons right">done</i>
          </button>';
          $promesses .= '</form></div><div class="divider"></div>';
          // SI PAS DE PRESTA
        }else{
          $promesses .= '<div class="divider"></div><h3 style="color:grey;text-align:left;" >La cagnotte est vide</h3>';
        }

      }
      $cagnotte = "";
      $go = $pochette->peutEtreValide() && $somme >= $total && $total !=0;
      // SI CAGNOTTE PAS FINI
      if($somme<$total || $total == 0){
        $lienPartage = $app->request->getRootUri()."/pochette/".$pochette->urlCagnotte;
        $cagnotte .=    "<div class=\"divider\"></div><p style=\"color:grey;\">Lien pour participer à la cagnotte : <a href=\"$lienPartage\">$lienPartage</a></p><div class=\"divider\"></div>";

      }
      // CAGNOTTE PEUT ETRE OFFERTE
      if($go){
        $lienPartage = $app->request->getRootUri()."/pochettesurprise/".$pochette->urlCadeau;
        $validation = "<div class=\"divider\"></div><p style=\"color:grey;\">Lien pour offir la pochette : <a href=\"$lienPartage\">$lienPartage</a></p><div class=\"divider\"></div>";
      }else{
        // MANGE UN TRUC POUR OFFRIR
        $validation = "<div class=\"divider\"></div><p style=\"color:grey;\">Pour offir la pochette, vous devez avoir au moins avoir 3 types de prestations différentes et que la cagnotte soit remplie.<div class=\"divider\"></div>";
      }
      // SI ON EST CREATEUR ET CAGNOTTE PAS FINI
      if($role==2 && !$go){
        $lienPartage = $app->request->getRootUri()."/pochette/".$pochette->urlGestion;
        $cagnotte .= "<p style=\"color:grey;\">Lien pour gérer à la cagnotte : <a href=\"$lienPartage\">$lienPartage</a></p><div class=\"divider\"></div>";
      }
      // SI ON EST DONNATEUR et qu'il y a des ccommandes
      if($role!=2 && $aCommande){
        $cagnotte .= "<div class=\"row grid\" >";
        foreach ($pp as $ppUnit) {
          $index = $ppUnit->idPrestation;
          $presta = Prestation::find($index);
          $cagnotte .= "<div class=\"col s12 m4\"><div class=\"card\">";
          $lienImagePresta = $lienImg.$presta->img;
          $cagnotte .= "<div class=\"card-image\"><img src=\"$lienImagePresta\"></div>";
          $cagnotte .= "</div></div>";
        }
        $cagnotte .= "</div>";
        // SI ON EST CREATEUR ET QUE Le truc n'est pas fini
      }else if($role==2 && !$go && $aCommande){


          $cagnotte .= "<table style=\"color:grey;margin-top:50px;\"><thead><tr><th>Titre</th><th>Prix</th><th>Heure</th><th>Supprimer</th></tr></thead><tbody>  ";
          foreach ($pp as $ppUnit) {
            $index = $ppUnit->idPrestation;
            $presta = Prestation::find($index);
            //$qt = PochettePresta::where('idPochette','=',$idPoch)->where('idPrestation', '=', $presta->id)->count();
            $cagnotte .= "<tr><td>".$presta->nom."</td>";
            $cagnotte .= "<td>".$presta->prix."</td>";
            $cagnotte .= "<td>".$ppUnit->heure."</td>";
            $cagnotte .= "<td><form action=\"\" method=\"post\">";
            $cagnotte .= '<input type="hidden" name="liaison" value="'.$ppUnit->idLiaison.'">';
            $cagnotte .= '<input type="hidden" name="faire" value="deleteLiaison">';
            $cagnotte .= '<button class="btn small red waves-effect waves-light" type="submit" name="action">
            <i class="material-icons right">close</i>
            </button>';
            $cagnotte .= "</form></tr>";
          }
          $cagnotte .= "</tbody></table>";
      }
      if($role==2){
        $boutonSuppr = <<<END
        <form action="" method="post">
          <input type="hidden" name="faire" value="deletePochette">
          <button class="btn red waves-effect waves-light" type="submit" name="action">Supprimer la pochette
            <i class="material-icons right">delete</i>
          </button>
        </form>
END;
      }else{
        $boutonSuppr = <<<END
        <form action="" method="post">
          <input type="hidden" name="faire" value="deconnecterPochette">
          <button class="btn waves-effect waves-light" type="submit" name="action">Quitter la pochette
            <i class="material-icons right">input</i>
          </button>
        </form>
END;
      }
      $html .= <<<END
  	<div id="index-banner" class="parallax-container notfullheight" >
      <div class="section no-pad-bot">
        <div class="container">
          <br><br>
          <h1 class="header center amber-text text-darken-3">
            $titre
            <br><br>
          </h1>
          <div class="row">
          <h5 style="color:grey;" class="center">$message</h5>
          </div>
          <br>
          <div class="row center">
          $promesses
          $validation
          $cagnotte
          <br>
          $boutonSuppr
          </div>
          <br>
          <br>
        </div>
      </div>
      <div class="parallax-container valign-wrapper">
          <div class="parallax"><img src="$lienImage" alt="img1"></div>
      </div>
    </div>


END;


  		return $html;
    }


    private function nouvellePochette(){


        $app = \Slim\Slim::getInstance();
        $uri = $app->request->getRootUri();

        $html = <<<END
	<div id="presta-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">

        <h1 class="header center amber-text text-darken-3">
          Créez votre pochette :

        </h1>

      </div>
    </div>
    <div class="parallax"><img src="$uri/public/img/imagepresta.jpg" alt="img1"></div>
  </div>


  <div class="container">
    <div class="section">

        <div id="formulaire pochette" class="col s12 m4">
          <div class="icon-block">

               <div class="">
            <div>

                <p class="flow-text">Pour créer votre pochette personnalisée il vous faut dans un premier temps remplir les champs suivants.</p>
            </div>
            <form action="" method="post">
                <div class="input-field">
                   <label for="destinataire">Nom du destinataire</label>
                   <input required id="desitnataire" type="text" class="form-control" name="destinataire" placeholder="Mon chéri"/>
                </div>
                <div class="input-field">
                   <label for="mPersonnalise">Message personnalisé</label>
                   <input required id="mPersonnalise" class="form-control" type="text" name="mPersonnalise" placeholder=""/>
                </div>
                <button class="btn waves-effect waves-light" type="submit" name="action">Créer
                <i class="material-icons right">send</i>
              </button>

            </form>
            </br>

            </div>



          </div>
        </div>




      </div>

    </div>
  </div>

  <div class="parallax-container valign-wrapper">
      <div class="parallax"><img src="$uri/public/img/imagepresta.jpg" alt="img1"></div>
  </div>
END;


        return $html;

    }

    private function paiementCagnotte() {
      $name = $this->data['pochette']->nom;
      $html = <<<END
              <div>
                <h2>
                  Participez à la cagnotte pour la pochette pour $name
                </h2>
                <p class = "flow-text">
                  Pour que la pochette surprise soit générée il faut que la cagnotte soit atteinte.
                </p>
                <form action="" method="post">
                  <div>
                    <label for="montant">
                      Montant du don :
                    </label>
                    <input id="montant" type="number" class="form-control" name="montant" placeholder="12">
                  </div>
                </form>
              </div>
END;
  return $html;
    }



}
