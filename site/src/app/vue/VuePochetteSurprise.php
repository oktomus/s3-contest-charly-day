<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11/02/2016
 * Time: 19:47
 */

namespace app\vue;

use app\models\Pochette;
use app\models\PochettePresta;


class VuePochetteSurprise
{
    private $data;
    private $content;

    /**
     * VuePochetteSurprise constructor.
     * @param null $data
     */
    public function __construct($data = null){
        $this->data = $data;
    }

    /**
     * Choisis le mode d'affichage
     * @param $type
     */
    public function render($type){
        switch ($type) {
            default:
            case 1:
                $content = $this->accueilPochette();
                break;

        }
        $vue = new VueIncludes();
        $vue->render(1);
        $vue->render(2);
        echo "<div class=\"container\">".$content."</div>";
        $vue->render(3);
        $vue->render(4);
    }

    /**
     * Affiche la vue lors de l'arrivée sur le lien
     */
    public function accueilPochette(){
        $app = \Slim\Slim::getInstance();
        $uri = $app->request->getRootUri();

        $urlCourante = $this->data[0];

        $contenu = " <p>Le contenu de votre pochette:<p>";

        $poch = Pochette::where("urlCadeau","=",$urlCourante)->get()->toArray();

        if (isset($poch[0])) {

          $pochP = PochettePresta::where("idPochette","=",$poch[0]['id'])->get()->toArray();
          foreach ($pochP as $value) {

            $contenu .= $this->pochetteSurpriseArticle($value['idPrestation']);
          }
        }


        $html = <<<END
	<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">

        <h2 class="header center amber-text text-darken-3">
          <img alt="logo" src="$uri/public/media/logo.png" class="responsive-img" width="500" height="250">
        </h2>


        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="$uri/public/background/background1_2.jpg" alt="img1" width="50%"></div>
  </div>


  <div class="container">
    <div class="section">

        <div id="experience" class="col s12 m4">
          <div class="icon-block">
            <h2 class="center amber-text text-darken-3"><i class="material-icons amber-text text-darken-3">grade</i></h2>
            <h3 class="center">Vous êtes chanceux ! </h3>

            <p class="center light"><b><h5>Vite ouvrez votre pochette pour découvrir les surprises qui vous attendent !  </h5></b></p>

          </div>
        </div>
    </div>
  </div>

  <div class="parallax-container valign-wrapper">
      <div class="parallax"><img src="$uri/public/background/background2_1.jpg" alt="img1"></div>
  </div>


  <div>
    $contenu
  </div>

<div class="parallax-container valign-wrapper">
  <div class="parallax"><img src="$uri/public/background/background3_1.jpg" alt="img1" width="50%"></div>
</div>

END;
        return $html;

    }

    /**
     * affiche la vue de la prestation en cours
     * @param $id
     */
    public function pochetteSurpriseArticle($id){
        $prest = \app\models\Prestation::find($id);

        $app = \Slim\Slim::getInstance();
        $lienImage  = $app->request->getRootUri()."/public/img/".$prest->img;
        $titre = $prest->nom;
        $texte = $prest->descr;
        $prix = $prest->prix;
        $uri = $app->request->getRootUri();
        $idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
        $special = "";
        $html = <<<END

      <div class="container">
        <br><br>
        <h2 class="header center amber-text text-darken-3">
          $titre
          <br><br>
        </h2>



    </div>


  <div class="container">
    <div class="section">



        <div id="experience" class="col s12 m4">
          <div class="icon-block">
            </br></br>
            <h5 class="center"> $texte </h5>
						<h5 class="center"> $special </h5>



          </div>
        </div>

		</br>
		</br>
<div class="parallax-container valign-wrapper">
      <div class="parallax"><img src="$lienImage" alt="img1"></div>
  </div></div></div>




END;
        return $html;
    }

}
