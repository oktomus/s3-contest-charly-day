<?php

namespace app\vue;

use \app\models\PochettePresta;
use \app\models\Prestation;
use \app\models\Type;
use \app\utils\GestionPochette;

class VuePrestation{

	private $data;
	private $content;

	public function __construct($data = null){
		$this->data = $data;
	}

	public function render($type){
		switch ($type) {
			default:
			case 1:
				$content = $this->catalogue();
				break;
			case 2:
				$content = $this->unePresta();
				break;
		}
    $message = "";
    if(isset($this->data) && isset($this->data['message']) && $this->data['message']){
       $message = "<div class=\"\"><div class=\"alert ". $this->data['message-type']."\" role=\"alert\">".$this->data['message-content']."</div></div>";
    }
		echo "<div class=\"container\">$message $content</div>";
	}

  private function catalogue(){
		$res = "<div class=\"row\"><div class=\"col s12 m12 l3\">";

		$res .= "<div class= \"row center-align\"><i class=\"material-icons\" style=\"font-size: 4rem\">label</i><h5>Filtrer<h5></div>";
		$res .= "<div class=\"input-field s12\">";
		$res .= "<form class=\"col s12\" action=\"\">";
		$res .= "<select name=\"pouet\">";
		$res .= "<option value=\"\" disabled selected> Type prestations </option>";
		$res .=	"<option value=\"tout\" selected> Toutes les prestations </option>";
		$res .=	"<option value=\"2\"> Activité </option>";
		$res .=	"<option value=\"3\"> Restauration </option>";
		$res .=	"<option value=\"4\"> Hébergement </option>";
		$res .=	"<option value=\"1\"> Attention </option>";
		$res .=	"</select>";
		  $res .= "<select name=\"prix\">";
		  $res .= "<option value=\"\" disabled selected>Tri par prix</option>";
		  $res .=	"<option value=\"0\" selected>Aucun tri</option>";
		  $res .=	"<option value=\"1\">Prix croissant</option>";
		  $res .=	"<option value=\"2\">Prix décroissant </option>";
		  $res .=	"</select>";
		$res .= "<button class=\"waves-effect waves-light btn col s12 m12 l12 black\" type=\"submit\" name=\"filtre\" value=\"filtre\"><i>Appliquer Filtre</button>";
		$res .= "</div>";
		$res .= "</div><div class=\"col s12 m12 l9\"><div class=\"grid row\">";
		foreach ($this->data["presta"] as $i => $presta) {
			//Cette classe ;
			//var_dump($presta->type);
			$type = Type::find($presta->type);
			$res .= "<div class=\"col s12 m4 $type->nom \"><div class=\"card\"><div class=\"card-image\">";
			$lienImage = $this->data["imageLink"].$presta->img;
			$res .= "<img src=\"$lienImage\"></div>";
			$res .= "<div class=\"card-content\"><span class=\"card-title activator grey-text text-darken-4\">";
			$res .= $presta->nom . "<i class=\"small material-icons\">+</i></span></div>";
			$res .= "<div class=\"card-reveal\"><span class=\"card-title grey-text text-darken-4\">".$presta->nom;
			$res .= "<i class=\"small material-icons\">-</i></span>";
			$res .= "<p class=\"prix\">".$presta->prix."€</p>	";
			$res .= "<p>".$presta->descr."</p>";
			if(in_array($presta->id, $this->data['prestaPrises'])){
				$nb = $this->data['prestaPrisesQuantite'][$presta->id];
				$res .= "<p>Vous avez commandé ".$nb." fois cette prestation.";
			}
			$res .= "<div class=\"card-action\">";
			$role = GestionPochette::roleUtilisateur();
			if($role==2){

				$res .= "<a href=\"".$this->data['addLink'].'/'.$presta->id."\">Ajouter à la pochette</a><br>";
			}
			if($role<2){
					$res .= "<a href=\"".$this->data['addLink'].'/'.$presta->id."\">Créer une pochette</a><br>";
			}
			$res .= "<a href=\"".$this->data['linkPage'].'/'.$presta->id."\">Voir plus de détails</a>";
			$res.= "</div></div></div></div>";
		}
		$res .= "</div></div></div>";
		if($this->data['nbPage']>1){

			$res .= "<ul class=\"pagination\">";
			$res .= "<li class=\"disabled\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>";
			for ($i=1; $i <= $this->data['nbPage'] ; $i++) {
				$link = $this->data['linkPage']."?".http_build_query(array("p"=>$i));
				if($i==$this->data['page']){
					$res .= "<li class=\"active\"><a href=\"$link\">$i</a></li>";
				}else{
					$res .= "<li class=\"waves-effect\"><a href=\"$link\">$i</a></li>";
				}
			}
			$res .= "<li class=\"disabled\"><a href=\"#!\"><i class=\"material-icons\">chevron_right</i></a></li></ul>";
		}
    return $res;
  }


	public function unePresta(){

		$prest = $this->data["presta"];
		$app = \Slim\Slim::getInstance();
		$lienImage  = $app->request->getRootUri()."/public/img/".$prest->img;
		$titre = $prest->nom;
		$texte = $prest->descr;
		$prix = $prest->prix;
		$uri = $app->request->getRootUri();
		$idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
		$special = "";
		if($idPoch>0){
			$nbPresta = PochettePresta::where('idPochette','=',$idPoch)->where('idPrestation', '=', $prest->id)->count();
			if ($nbPresta > 0){
				$special .= "Vous avez $nbPresta fois cette prestation dans votre pochette.";
			}
		}
		$ajout = "";
		$lienAjout =  $app->request->getRootUri()."/ajouterPrestation/".$prest->id;
		$role = GestionPochette::roleUtilisateur();
		if($role==2){
			$ajout = '  <div class="row center">
          <a href="'.$lienAjout.'" id="download-button" class="btn-large waves-effect waves-light amber darken-3">Ajouter à ma collection</a>
        </div>';
		}
		if($role<2){
			$ajout = '  <div class="row center">
          <a href="'.$lienAjout.'" id="download-button" class="btn-large waves-effect waves-light amber darken-3">Créer une pochette</a>
        </div>';
		}

		$html = <<<END
	<div id="presta-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center amber-text text-darken-3">
          $titre
          <br><br>
        </h1>

        $ajout
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="$uri/public/img/imagepresta.jpg" alt="img1"></div>
  </div>


  <div class="container">
    <div class="section">



        <div id="experience" class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">shopping_basket</i></h2>
            <h3 class="center">Pour seulement $prix € ! </h3>
            </br></br>
            <h5 class="center"> $texte </h5>
						<h5 class="center"> $special </h5>



          </div>
        </div>

		</br>
		</br>



      </div>

    </div>
  </div>

  <div class="parallax-container valign-wrapper">
      <div class="parallax"><img src="$lienImage" alt="img1"></div>
  </div>
END;


		return $html;
	}

}
