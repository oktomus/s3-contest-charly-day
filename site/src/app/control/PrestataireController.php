<?php

namespace app\control;

use app\utils\HttpRequest;
use app\vue\VueIncludes;
use app\vue\VuePrestation;
use app\vue\VuePrestataire;
use app\utils\Authentification;
use app\utils\ConnexionException;
use app\models\Type;
use app\models\Prestataire;
use app\models\Prestation;


class PrestataireController extends AbstractController{


	public function __construct(HttpRequest $req = null){
    	parent::__construct($req);
  	}

	public function genererFormulaireCoPrestataire($mess = null) {
		$params = [];
		$params['message'] = $mess;
		$vue = new VuePrestataire($params);
		$vueIn = new VueIncludes();
		$vueIn->render(1);
		$vueIn->render(2);
		$vue->render(1);
		$vueIn->render(3);
		$vueIn->render(4);
	}

	public function genererFormAjoutPres($mess = null) {
		$params = [];
		$params['message'] = $mess;
		$types = Type::all()->toArray();
		foreach ($types as $value) {
			$params[] = $value;
		}
		$vue = new VuePrestataire($params);
		$vueIn = new VueIncludes();
		$vueIn->render(1);
		$vueIn->render(2);
		$vue->render(2);
		$vueIn->render(3);
		$vueIn->render(4);
	}

	public function connecterPrestataire() {
		if (!isset($_SESSION['email'])) {
			$app = \Slim\Slim::getInstance();
			$params = $app->request->post();

			$email = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
			$mdp = filter_var($params['mdp'], FILTER_SANITIZE_STRING);

			try {

				Authentification::authenticate($email, $mdp);

				$this->genererFormAjoutPres();
			} catch(ConnexionException $ae) {
				$this->genererFormulaireCoPrestataire($ae->getMessage());
			}
		} else {
			$this->genererFormAjoutPres("Vous êtes déjà connecté, vous n'auriez pas du accéder à cette page.");
		}
	}

	public function deconnecterPrestataire() {
		try {
			Authentification::checkSession();

			Authentification::deconnexion();
			$c = new AppController();
			$c->accueil();
		} catch(ConnexionException $e) {
			$this->genererFormulaireCoPrestataire($e->getMessage());
		}
	}

	public function ajouterNouvellePrestation() {
		
		try {
			Authentification::checkSession();

			$app = \Slim\Slim::getInstance();
			$params = $app->request->post();
				
			$nom = filter_var($params['nom'], FILTER_SANITIZE_STRING);
			$type = $params['type'];
			$descr = filter_var($params['description'], FILTER_SANITIZE_STRING);
			$img = filter_var($params['img'], FILTER_SANITIZE_STRING);
			$prix = filter_var($params['prix'], FILTER_SANITIZE_NUMBER_FLOAT);

			$pres = new Prestation();
			$pres->nom = $nom;
			$pres->descr = $descr;
			$pres->img = $_SESSION['email'].$img;
			$pres->prix = $prix;
			$pres->type = $type;
			$pres->email = $_SESSION['email'];

			$pres->save();

			$data = [];
			$data['nom'] = $nom; 
			$data['descr'] = $descr;
			$data['prix'] = $prix;
			$data['type'] = Type::find($type)->nom;
			$vue = new VuePrestataire($data);
			$vueIn = new VueIncludes();
			$vueIn->render(1);
			$vueIn->render(2);
			$vue->render(3);
			$vueIn->render(3);
			$vueIn->render(4);

		} catch(ConnexionException $e) {
			$this->genererFormulaireCoPrestataire($e->getMessage());
		}
	}
}