<?php
/**
 * Created by PhpStorm.
 * User: Tim
 * Date: 11/02/2016
 * Time: 10:29
 */

namespace app\control;

use app\models\Prestation;
use app\models\Pochette;
use app\models\Promesse;
use app\models\PochettePresta;
use \app\vue\VuePochette;

use app\utils\HttpRequest;
use app\utils\GestionPochette;

class PochetteController extends AbstractController {

    public function __construct(HttpRequest $req = null)
    {
        parent::__construct($req);
    }

    public function pochette(){
      $idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
      if($idPoch <1){
        $this->creePochette();
      }else{
        if(isset($this->req->post['faire']) && $this->req->post['faire']=='deleteLiaison'){
            PochettePresta::find($this->req->post['liaison'])->delete();
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('pochette'));
        }else if(isset($this->req->post['faire']) && $this->req->post['faire']=='ajouterDon'){
            $promesse = new Promesse();
            $promesse->nom = filter_var($this->req->post['nom'], FILTER_SANITIZE_STRING);
            $promesse->somme = $this->req->post['montant'];
            $promesse->idPochette = $this->req->post['pochette'];
            $promesse->save();
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('pochette'));
        }else if(isset($this->req->post['faire']) && $this->req->post['faire']=='deletePochette'){
            Promesse::where('idPochette', '=', $idPoch)->delete();
            PochettePresta::where('idPochette','=',$idPoch)->delete();
            Pochette::find($idPoch)->delete();
            GestionPochette::deconnexion();
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('pochette'));
          }else if(isset($this->req->post['faire']) && $this->req->post['faire']=='deconnecterPochette'){
              GestionPochette::deconnexion();
              $app = \Slim\Slim::getInstance();
              $app->redirect($app->urlFor('pochette'));
        }else{
          $po = Pochette::find($idPoch);
          $data = array();
          $app = \Slim\Slim::getInstance();
          $data['public'] = $app->request->getRootUri()."/public";
          $data['lienImage'] = $app->request->getRootUri();
          $data['pochette'] = $po;
          $vue = new VuePochette($data);
          $vue->render(2);
        }
      }

    }

    public function creePochette() {
        $vue = new VuePochette();
        if(isset($this->req->post['destinataire'])) {
          $po = new Pochette();
          $po->nom = filter_var($this->req->post['destinataire'], FILTER_SANITIZE_STRING);
          $po->message = filter_var($this->req->post['mPersonnalise'], FILTER_SANITIZE_STRING);
          GestionPochette::creePochette($po);

          $app = \Slim\Slim::getInstance();
          $app->redirect($app->urlFor('pochette'));
        } else {
          $vue->render(1);
        }
    }

    public function ajouterPrestation($id){
      $idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
      if($idPoch<1){
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor('pochette'));
      }else{
        if (!is_null($id)){
          $pres = Prestation::whereRaw('id = ?',array($id))->get();
          if (count($pres[0])==1){
            $pochettePresta = new PochettePresta();
            $pochettePresta->idPrestation = $id;
            $pochettePresta->idPochette = $idPoch;
            $pochettePresta->heure = "65";
            $pochettePresta->save();
          }

        }
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor('catalogue'));

      }


    }


    public function retirerPrestation($id){

    }

    public function devoiler($url) {
        $role = 0;
        $pGestion = Pochette::where('urlGestion', '=', $url)->first();
        $pCagnotte = Pochette::where('urlCagnotte', '=', $url)->first();
        if(!is_null($pGestion)){
            $po = $pGestion->id;

            $role = 2;
            GestionPochette::loadProfile($po, $role);
        }
        else if(!is_null($pCagnotte)) {
            $po = $pCagnotte->id;
            $role = 4;
            GestionPochette::loadProfile($po, $role);
        }
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor('pochette'));
    }
}
