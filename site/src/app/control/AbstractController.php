<?php

namespace app\control;
use \app\utils\HttpRequest;

class AbstractController{

	protected $req;

	public function __construct(HttpRequest $req = null){
		$this->req = $req;
	}

}
