<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11/02/2016
 * Time: 19:40
 */

namespace app\control;

use app\vue\VuePochetteSurprise;
use app\utils\HttpRequest;

class PochetteSurpriseController extends AbstractController
{
    public function __construct(HttpRequest $req = null)
    {
        parent::__construct($req);
    }

    /**
     * pour afficher la vue lorsque le gars clique le lien un truc du genre " coucou "
     */
    public function afficherAccueilPochette($url){
        $vue = new VuePochetteSurprise(array($url));


        $vue ->render(1);



    }

    /**
     * Affiche les presta par la vue mais c'est caca là
     */
    public function afficherPrestaSurprise(){
        $vue = new VuePochetteSurprise();
        $v = new AppController($this->req);

        $v->head();
        $v->menu();

        $vue ->render(2);


        $v->pieDePage();
        $v->foot();
    }

}