<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 11/02/2016
 * Time: 10:01
 */

namespace app\control;


use app\models\Prestation;
use app\vue\VuePrestation;

class PrestationController extends AbstractController
{

    /**
     * PrestationController constructor.
     * @param HttpRequest|null $req
     */
    public function __construct(HttpRequest $req = null){
        parent::__construct($req);
    }

    /**
     * Affiche les détails d'une prestation unique qui est appelé quand on clique dessus
     * @param $id
     */
    public function detailsPrestation($id){
        $vueApp = new AppController();
        $vueApp->head();
        $vueApp->menu();
        $id = intval($id);
        $presta = Prestation::where('id', '=', $id)->first();

            if(is_null($presta) || !isset($presta)){
              $app = \Slim\Slim::getInstance();
              $app->redirect($app->urlFor('catalogue'));
                $vue-> render(1);
            }else{
                $tab = ["presta" => $presta];

                //$tab["presta"] = $presta;
                $vue = new VuePrestation($tab);
                $vue-> render(2);
            }
        $vueApp->pieDePage();
        $vueApp->foot();
       // }
        if(isset($message)){
            echo $message;
        }

    }


}
