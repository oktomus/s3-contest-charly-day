<?php

namespace app\control;


use app\utils\HttpRequest;
use app\vue\VueApp;
use app\vue\VueIndex;
use app\vue\VueIncludes;
use app\vue\VuePrestation;
use app\models\Prestation;
use app\models\PochettePresta;
use app\models\Type;



class AppController extends AbstractController{

  public function __construct(HttpRequest $req = null){
    parent::__construct($req);
  }

  public function notFound(){
    $this->head();
    $this->menu();
    $vue = new VueApp();
    $vue->render(2);
    $this->pieDePage();
    $this->foot();
  }

  public function accueil() {
    $this->head();
    $datas = [];
    $types = Type::all()->toArray();
    foreach ($types as $value) {
      $presta = Prestation::where('type','=',$value['id'])->get()->toArray();
      $datas[$value['nom']] = $presta[0];
    }
    $vue = new VueIndex($datas);
    $vue->render(1);
    $this->pieDePage();
    $this->foot();
  }

  public function home(){
    $this->head();
    $this->menu();
    $vue = new VueApp();
    $vue->render(1);
    $this->pieDePage();
    $this->foot();
  }

  public function catalogue(){
    $this->head();
    $this->menu();

    $app = \Slim\Slim::getInstance();

    $nbPresta = Prestation::count();
    $nbParPage = 30;
    $nbPage = $nbPresta/$nbParPage;
    if(isset($this->req->get["p"]))
    {
      $page = $this->req->get["p"];
    }else{
      $page = 1;
    }
    if($page<0 || $page>$nbPresta/$nbParPage) $page=1;

    $datas = [];


    if(count($this->req->get)>0) {

      if (isset($this->req->get['prix'])&&($this->req->get['prix']=="0")){
        switch ($this->req->get['pouet']) {
          case "tout":
            $datas["presta"] = Prestation::skip(($page - 1) * $nbParPage)->take($nbParPage)->get();
            break;
          default:
            $type = $this->req->get['pouet'];
            $temp = Prestation::where('type', '=', $type);
            $datas["presta"] = $temp->skip(($page - 1) * $nbParPage)->take($nbParPage)->get();
            break;
        }
      }else if ($this->req->get['prix']=="1"){
        switch ($this->req->get['pouet']) {
          case "tout":
            $datas["presta"] = Prestation::skip(($page - 1) * $nbParPage)->take($nbParPage)->get();
            break;
          default:
            $type = $this->req->get['pouet'];
            $temp = Prestation::where('type', '=', $type)->orderBy('prix','ASC');
            $datas["presta"] = $temp->skip(($page - 1) * $nbParPage)->take($nbParPage)->get();
            break;
        }
      }else if ($this->req->get['prix']=="2"){
        switch ($this->req->get['pouet']) {
          case "tout":
            $datas["presta"] = Prestation::skip(($page - 1) * $nbParPage)->take($nbParPage)->get();
            break;
          default:
            $type = $this->req->get['pouet'];
            $temp = Prestation::where('type', '=', $type)->orderBy('prix','DESC')->orderBy('prix','DESC');
            $datas["presta"] = $temp->skip(($page - 1) * $nbParPage)->take($nbParPage)->get();
            break;
        }
      }




    } else {
      $datas["presta"] = Prestation::skip(($page-1)*$nbParPage)->take($nbParPage)->get();
    }
    $datas["page"] = $page;
    $datas["nbPage"] = $nbPage;
    $datas["imageLink"] = $app->request->getRootUri()."/public/img/";
    $datas["linkPage"] = $app->urlFor('catalogue');
    $datas['addLink'] =$app->request->getRootUri()."/ajouterPrestation";
    $idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
    $datas['prestaPrises'] = array();
    $datas['prestaPrisesQuantite'] = array();
    if($idPoch > 0){
      $prestas = PochettePresta::where('idPochette','=',$idPoch)->get();
      foreach ($prestas as $p) {
        array_push($datas['prestaPrises'], $p->idPrestation);
        $nbPresta = PochettePresta::where('idPochette','=',$idPoch)->where('idPrestation', '=', $p->idPrestation)->count();
        $datas['prestaPrisesQuantite'][$p->idPrestation] = $nbPresta;
      }
    }

    $vue = new VuePrestation($datas);

    $vue->render(1);
    $this->pieDePage();
    $this->foot();

  }

  public function head(){
    $vue = new VueIncludes();
    $vue->render(1);
  }

  public function menu(){
    $vue = new VueIncludes();
    $vue->render(2);
  }

  public function pieDePage(){
    $vue = new VueIncludes();
    $vue->render(3);
  }

  public function foot(){
    $vue = new VueIncludes();
    $vue->render(4);
  }



}
