<?php

namespace app\models;

class Pochette extends \Illuminate\Database\Eloquent\Model {
    protected $table ='pochette';
    protected $primaryKey ='id';
    public $timestamps = false;

    public function pochettesPrestations() {
        return $this->hasMany('\app\models\PochettePresta', 'idPochette');
    }

    public function peutEtreValide(){
         $idPoch = \app\utils\GestionPochette::pochetteUtilisateur();
         $pp = PochettePresta::where('idPochette','=',$idPoch)->get();
         $nbTypeDif = 0;
         $types = array();
         foreach($pp as $p){
           $presta = Prestation::find($p->idPrestation);
           $type = Type::find($presta->type)->nom;
           if(!in_array($type, $types)){
             array_push($types, $type);
             $nbTypeDif++;
           }
         }
         if($nbTypeDif > 2){
           return true;
         }
         return false;
    }


}
