<?php

namespace app\models;

class Type extends \Illuminate\Database\Eloquent\Model {
	protected $table ='type';
	protected $primaryKey ='id' ;
	public $timestamps = false;

  public function prestations(){
    return $this->belongsTo('\app\models\Prestation', 'type');
  }

}
