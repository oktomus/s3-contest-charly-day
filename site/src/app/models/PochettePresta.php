<?php

namespace app\models;

class PochettePresta extends \Illuminate\Database\Eloquent\Model {
    protected $table ='pochettepresta';
    protected $primaryKey ='idLiaison';
    public $timestamps = false;

    public function pochette() {
        return $this->belongsTo('\app\models\Pochette','id');
    }

    public function prestations() {
        return $this->hasMany('\app\models\Prestation','id');
    }

}
