<?php

namespace app\models;

class Promesse extends \Illuminate\Database\Eloquent\Model {
	protected $table ='promesse';
	protected $primaryKey ='id' ;
	public $timestamps = false;

}
