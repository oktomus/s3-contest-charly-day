<?php

namespace app\models;

class Prestataire extends \Illuminate\Database\Eloquent\Model {
	protected $table ='prestataire';
	protected $primaryKey ='email' ;
	public $timestamps = false;

}
