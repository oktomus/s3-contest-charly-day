<?php

namespace app\models;

class Prestation extends \Illuminate\Database\Eloquent\Model {
	protected $table ='prestation';
	protected $primaryKey ='id' ;
	public $timestamps = false;

  public function types(){
    return $this->belongsTo('\app\models\Type', 'id');
  }

  public function pochettePrestations() {
      return $this->belongsTo('\app\models\PochettePresta', 'idPrestation');
  }

}
