<?php
session_start();
require('vendor/autoload.php');
use \app\control\AppController;
use \conf\DbConf;
use \app\utils\HttpRequest;
use \app\control\PrestationController;
use \app\control\PrestataireController;
use \app\control\PochetteController;
use \app\control\PochetteSurpriseController;

DbConf::init('xfr/db.app.conf.ini');

$http = new HttpRequest();

$app = new \Slim\Slim([
	'templates.path' => 'includes'
	]);

$app->notFound(function () use ($app) {
	$c = new AppController();
	$c->notFound();
});

$app->get('/', function () use($app) {
	$c = new AppController();
	$c->accueil();
})->name('home');


$app->get('/catalogue', function () use($http, $app){
	$c = new AppController($http);
	$c->catalogue();
})->name('catalogue')->via(array("GET","POST"));


$app->get('/catalogue/:id', function ($id) use($http, $app){
	//faire un appel vers prestation controler puis dedans appcontroler ou faire un truc dans le genre pour avoir le header

	$c =  new PrestationController();
	$c->detailsPrestation($id);
	//$c = new AppController($http);
	//$c->catalogue();
});

$app->map('/ajouterPrestation/:id', function($id) use($app){
	$c = new PochetteController();
	$c->ajouterPrestation($id);
})->name('ajouterPrestation')->via(array("GET","POST"));

$app->map('/pochette', function () use($http, $app){
	$c = new PochetteController($http);
	$c->pochette();
})->name('pochette')->via(array("GET","POST"));

$app->get('/pochette/:url', function($url) use($http, $app){
	$c = new PochetteController();
	$c->devoiler($url);
})->name('urlpochette');


$app->get('/pochettesurprise/:url', function($url) use($http, $app){
	$c = new PochetteSurpriseController($http);
	$c->afficherAccueilPochette($url);
})->name('pochettesurprise');


$app->get('/prestataire', function() use ($app) {
	$c = new PrestataireController();
	$c->genererFormulaireCoPrestataire();
})->name('prestataire');

$app->post('/connecterPrestataire', function() use ($app) {
	$c = new PrestataireController();
	$c->connecterPrestataire();
})->name('connecter');

$app->get('/deconnecterPrestataire', function() use ($app) {
	$c = new PrestataireController();
	$c->deconnecterPrestataire();
})->name('deconnecter');

$app->get('/ajouterNouvellePrestation', function() use($app){
	$c = new PrestataireController();
	$c->genererFormAjoutPres();
})->name('ajouterNouvellePrestation');

$app->post('/ajouter', function() use($app){
	$c = new PrestataireController();
	$c->ajouterNouvellePrestation();
})->name('ajouter');


$app->run();





// OBSELETE $app->render('footer.php');
